package com.saba;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class  SortingApp
{
    private static final Logger logger = LogManager.getLogger(SortingApp.class);

   /**
   * This is Sort Helper function that does the main functionality
   * */
    private static List<Integer>  sortHelper(List<Integer> array){
        logger.info("Actual Sorting Step get started.");

        for (int sticker = 0; sticker < array.size(); sticker += 1) {
            int j = sticker;
            int minimum = array.get(j);
            for (int i = sticker; i < array.size(); i += 1) {
                if (array.get(i) < minimum) {
                    j = i;
                    minimum = array.get(j);
                }
            }
            int temp = array.get(sticker);
            array.set(sticker, minimum);
            array.set(j, temp);
        }
        return array;
    }

    /**
     * Then follow the sorting implementations.
     * */
    public static void main(String[] args){
        logger.info("Sorting App started.");
        if (args == null) {
            throw new IllegalArgumentException("args[] cannot be null");
        }
        if (args.length > 0) {
            List<Long> array = new ArrayList<Long>();


            for (String txt : args) {
                try {
                    if(txt == null){
                        logger.error("null values will be omitted");
                        continue;
                    }
                    long number = Long.parseLong(txt);
                    array.add(number);
                } catch (NumberFormatException e) {
                    logger.error("Invalid format for the integer number");
                    throw new IllegalArgumentException();
                }
            }

            Collections.sort(array);

            logger.info("Sorted numbers:" + array.toString());
            for (Long integer : array) {
                System.out.print(integer + "\t");
            }
            System.out.println();
            return;
        }

        logger.warn("No numbers provided.");

        //SortingApp.printResourceFile();
    }

    private static void printResourceFile() {
        try {
            InputStream inputStream = SortingApp.class.getResourceAsStream("C:\\Users\\My computer\\Libraries\\Documents\\Programming Related Tasks\\Java Projects\\sorting-app-saba-konjaria\\sorting-app-saba-konjaria\\src\\main\\resources\\input.txt");
            String fileContent = IOUtils.toString(inputStream, "UTF-8");
            logger.info("Resource file content:\n{}", fileContent);
        } catch (Exception e) {
            logger.error("Failed to read resource file.", e);
        }
    }

}
