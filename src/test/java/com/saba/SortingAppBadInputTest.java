package com.saba;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class SortingAppBadInputTest {

    @Test(expected = IllegalArgumentException.class)
    public void testNullArg() {
        SortingApp.main(null);
    }

    @Test
    public void testNullSingleElementArray() {
        String[] nullArray = new String[]{null};
        readingFromConsole(nullArray, new Long[]{});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMalformedSingleElementArray() {
        String[] malformedArray = new String[]{"%"};
        SortingApp.main(malformedArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNonDigitalInput(){
        SortingApp.main(new String[]{"Another try"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPositiveFractionalInput(){
         SortingApp.main(new String[]{"9", "9.0"});
    }

     @Test(expected = IllegalArgumentException.class)
    public void testNegativeFractionalInput(){
         SortingApp.main(new String[]{"-9", "-9.000"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroAsFractionalInput(){
        SortingApp.main(new String[]{"-0.0", "0.0"});
    }

    @Test
    public void testNullSomewhereBetween(){
        readingFromConsole(new String[]{"10", null, "80", "11", null}, new Long[]{10L, 11L, 80L});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMixedDataTypes(){
        SortingApp.main(new String[]{"null", "90", "false", "true","<", ">"});
    }
    private void readingFromConsole(String[] args, Long[] output) {
        // Redirect standard output to capture the printed values
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));

        try {
            SortingApp.main(args);
            // Retrieve the printed output
            String[] outputLines = outputStream.toString().trim().split("\n");
            for (String outputLine : outputLines) {
                System.out.println(outputLine);
            }
            // Get the sorted numbers from the last line of output
            String[] sortedNumbers = outputLines[outputLines.length - 1].split("\t");

            // Convert the sorted numbers to a list of integers
            List<Long> actualSortedValues = Arrays.stream(sortedNumbers)
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
            // Define the expected sorted values
            List<Long> expectedSortedValues = Arrays.asList(output);

            // Compare the expected and actual sorted values
            assertEquals(expectedSortedValues, actualSortedValues);
        } catch (Exception e) {
            System.out.println("Ooops... Something went wrong");
        } finally {
            System.setOut(originalOut);
        }
    }

}
