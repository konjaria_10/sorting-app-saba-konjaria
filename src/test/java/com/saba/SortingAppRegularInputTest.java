package com.saba;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import static org.junit.Assert.assertEquals;

public class SortingAppRegularInputTest {


   @Test
   public void testSingleElementArray(){
       readingFromConsole(new String[]{"1"}, new Long[]{1L});
   }


    @Test
    public void testSortedMultipleElementsArray(){
        readingFromConsole(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, new Long[]{1L, 2L,3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L});
    }

    @Test
    public void testUnSortedMultipleElementsArray(){
        Long[] sorted_arr = new Long[]{1L, 2L,3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L};
        String[] arr = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        Collections.shuffle(Arrays.asList(arr));
        readingFromConsole(arr, sorted_arr);
    }

    @Test
    public void testExtremeValues() {
        Long[] sorted_arr = new Long[]{Long.MIN_VALUE, Long.MAX_VALUE};
        String[] arr = new String[]{String.valueOf(Long.MIN_VALUE), String.valueOf(Long.MAX_VALUE)};

        Collections.shuffle(Arrays.asList(arr));
        readingFromConsole(arr, sorted_arr);
    }
    private void readingFromConsole(String[] args, Long[] output) {
        // Redirect standard output to capture the printed values
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));

        try {
            SortingApp.main(args);
            // Retrieve the printed output
            String[] outputLines = outputStream.toString().trim().split("\n");
            for (String outputLine : outputLines) {
                System.out.println(outputLine);
            }
            // Get the sorted numbers from the last line of output
            String[] sortedNumbers = outputLines[outputLines.length - 1].split("\t");

            // Convert the sorted numbers to a list of integers
            List<Long> actualSortedValues = Arrays.stream(sortedNumbers)
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
            // Define the expected sorted values
            List<Long> expectedSortedValues = Arrays.asList(output);

            // Compare the expected and actual sorted values
            assertEquals(expectedSortedValues, actualSortedValues);
        } catch (Exception e) {
            System.out.println("Ooops... Something went wrong");
        } finally {
            System.setOut(originalOut);
        }
    }


}