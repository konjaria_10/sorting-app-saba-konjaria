package com.saba;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import static org.junit.Assert.assertEquals;

public class SortingAppEdgeCasesTest {

    @Test
    public void testEmptyCase() {
        String[] arr = new String[0];
        SortingApp.main(arr);
        // Length of an array should not be modified.
        assertEquals(0, arr.length);
    }

    @Test
    public void testSingleElementArray() {
        String[] singleElementArray = new String[]{"1"};
        readingFromConsole(singleElementArray, new Long[]{1L});
    }

    @Test
    public void testTenElementArray() {
        Long[] sorted_arr = new Long[]{1L, 2L,3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L};
        String[] arr = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        Collections.shuffle(Arrays.asList(arr));
        readingFromConsole(arr, sorted_arr);
    }

    @Test
    public void testMoreThanTenElementArray() {
        Long[] sorted_arr = new Long[1000];
        String[] arr = new String[1000];

        for (int i = 0; i < 1000; i++) {
            sorted_arr[i] = (long) i+1;
            arr[i] = String.valueOf(i+1);
        }

        Collections.shuffle(Arrays.asList(arr));
        readingFromConsole(arr, sorted_arr);
    }




    private void readingFromConsole(String[] args, Long[] output) {
        // Redirect standard output to capture the printed values
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));

        try {
            SortingApp.main(args);
            // Retrieve the printed output
            String[] outputLines = outputStream.toString().trim().split("\n");
            for (String outputLine : outputLines) {
                System.out.println(outputLine);
            }
            // Get the sorted numbers from the last line of output
            String[] sortedNumbers = outputLines[outputLines.length - 1].split("\t");

            // Convert the sorted numbers to a list of integers
            List<Long> actualSortedValues = Arrays.stream(sortedNumbers)
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
            // Define the expected sorted values
            List<Long> expectedSortedValues = Arrays.asList(output);

            // Compare the expected and actual sorted values
            assertEquals(expectedSortedValues, actualSortedValues);
        } catch (Exception e) {
            System.out.println("Ooops... Something went wrong");
        } finally {
            System.setOut(originalOut);
        }
    }




}
