package com.saba;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingAppCSVParameterizedSource {
    private String[] input;
    private Long[] output;

    public SortingAppCSVParameterizedSource(String[] input, Long[] output) {
        this.input = input;
        this.output = output;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Long[] sorted_arr = new Long[1000];
        String[] arr = new String[1000];

        for (int i = 0; i < 1000; i++) {
            sorted_arr[i] = (long) i+1;
            arr[i] = String.valueOf(i+1);
        } // more than 10 elements
        Collections.shuffle(Arrays.asList(arr));
        return Arrays.asList(new Object[][] {
                {new String[0], new Long[0]},
                {new String[]{"1"}, new Long[]{1L}},
                {new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, new Long[]{1L, 2L,3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L}},
                {arr, sorted_arr}
        });
    }

    @Test
    public void testNoRootsCase() {
        readingFromConsole();
    }
    void readingFromConsole() {
        // Redirect standard output to capture the printed values
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outputStream));

        try {
            SortingApp.main(this.input);
            // Retrieve the printed output
            String[] outputLines = outputStream.toString().trim().split("\n");
            for (String outputLine : outputLines) {
                System.out.println(outputLine);
            }
            // Get the sorted numbers from the last line of output
            String[] sortedNumbers = outputLines[outputLines.length - 1].split("\t");

            // Convert the sorted numbers to a list of integers
            List<Long> actualSortedValues = Arrays.stream(sortedNumbers)
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
            // Define the expected sorted values
            List<Long> expectedSortedValues = Arrays.asList(this.output);

            // Compare the expected and actual sorted values
            assertEquals(expectedSortedValues, actualSortedValues);
        } catch (Exception e) {
            System.out.println("Ooops... Something went wrong");
        } finally {
            System.setOut(originalOut);
        }
    }

}
